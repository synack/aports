# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=glslang
pkgver=1.3.211.0
pkgrel=0
pkgdesc="Khronos reference front-end for GLSL, ESSL, and sample SPIR-V generator"
url="https://github.com/KhronosGroup/glslang"
arch="all"
license="BSD-3-Clause"
depends_dev="$pkgname"
makedepends="cmake samurai python3 bison spirv-tools-dev"
checkdepends="bash gmock gtest"
subpackages="$pkgname-static $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/glslang/archive/sdk-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-sdk-$pkgver"

case "$CARCH" in
s390x) options="$options !check" ;; # testsuite seems to fail on big endian
esac

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build-shared -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_SHARED_LIBS=ON \
		-DENABLE_CTEST="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build-shared

	sed -i '/add_library(glslang-default-resource-limits/ s/$/ STATIC/' StandAlone/CMakeLists.txt
	cmake -B build-static -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_SHARED_LIBS=OFF \
		-DENABLE_CTEST="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build-static
}

check() {
	msg "Testing shared version of glslang"
	ctest --test-dir build-shared --output-on-failure

	msg "Testing static version of glslang"
	ctest --test-dir build-static --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build-shared
	DESTDIR="$pkgdir" cmake --install build-static
}

sha512sums="
d2dce0938f77d1c7736db0ba97e557fd79f4efc944a489662bbce66bee923a31554ef73099087853779bba72fc2aae53e4bb2d6fb0a69cb13e4f24c5a789bb64  glslang-1.3.211.0.tar.gz
"
