# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kirigami-gallery
pkgver=22.04.1
pkgrel=0
arch="all !armhf" # armhf blocked by kirigami2 -> qt5-qtdeclarative
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
pkgdesc="Gallery application built using Kirigami"
license="LGPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kirigami2-dev
	kitemmodels-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5bf82d68cdc2b0bfb8eabb1a35919b7818f84e4d32b9bcdef7eb10e7d9af917173824b8f39b491d9e212200038d5ba018a8a253ce727cd95196649b339cb89b5  kirigami-gallery-22.04.1.tar.xz
"
