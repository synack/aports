# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaccounts-providers
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later"
depends="
	signon-plugin-oauth2
	"
makedepends="
	extra-cmake-modules
	intltool
	kaccounts-integration-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kpackage-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtwebengine-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-providers-$pkgver.tar.xz"
options="!check" # No tests
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e1a4ce98af9eb2e6d7005fc023d6c57a26de66613e060cfb1ebd6f24879ad6b1e48c25a9b58c347ce83d1b9eb06c30d119d4ddce6a7e898c8459667753db5d5c  kaccounts-providers-22.04.1.tar.xz
"
