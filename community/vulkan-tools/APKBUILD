# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-tools
pkgver=1.3.211.0
pkgrel=0
arch="all"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan Utilities and Tools"
license="Apache-2.0"
depends="vulkan-loader"
makedepends="
	cmake
	glslang-dev
	libx11-dev
	libxrandr-dev
	python3
	samurai
	vulkan-headers
	vulkan-loader-dev
	wayland-dev
	wayland-protocols-dev
	"
source="https://github.com/KhronosGroup/Vulkan-Tools/archive/sdk-$pkgver/vulkan-tools-sdk-$pkgver.tar.gz"
options="!check" # No tests
builddir="$srcdir/Vulkan-Tools-sdk-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_DATADIR=/usr/share \
		-DCMAKE_SKIP_RPATH=True \
		-DBUILD_CUBE=ON \
		-DBUILD_VULKANINFO=ON \
		-DGLSLANG_INSTALL_DIR=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
36be2970dd42e12bf6f6644bc1b14f102af36f1297c33fadddca470b65d7d324532b20c071d2f0dd790d556cd99012a4fb80f185db6165ed3f09d1fd7e5c50e7  vulkan-tools-sdk-1.3.211.0.tar.gz
"
