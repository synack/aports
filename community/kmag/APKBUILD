# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmag
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kmag"
pkgdesc="A screen magnifier"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmag-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1dec7f42e8781a320036f169e7aeba07c9cd1f53104e13660f02916f4ba42abbda2a13b12698ca9bd91f48da31953f5d2f5b85b59681f8109e90a998296e76e2  kmag-22.04.1.tar.xz
"
