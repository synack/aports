# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knights
pkgver=22.04.1
pkgrel=0
pkgdesc="Chess board by KDE with XBoard protocol support"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/games/knights/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kio-dev
	kplotting-dev
	ktextwidgets-dev
	kwallet-dev
	kxmlgui-dev
	libkdegames-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/knights-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
325263c4d69b3bfb505b9a01f8c9d65aa0f7b9d49283043f063ccd8f9563d9a54bbb0c322f6543df3cf603a570a72deaf8b5d1927a8626e7bcdac240e8fabdcd  knights-22.04.1.tar.xz
"
