# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdenetwork-filesharing
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# armhf, s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://www.kde.org/applications/internet/"
pkgdesc="Properties dialog plugin to share a directory with the local network"
license="GPL-2.0-only OR GPL-3.0-only"
depends="samba"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kcoreaddons-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenetwork-filesharing-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DSAMBA_INSTALL=OFF
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bdd7eafae0e433e1820661e2d1fc64a08d98c9e2fccaf4bed22d7f5604bd5bf66f13c412e458f59aeeea6bca56719525fb9a312b62f5a163558e87869a7d9c39  kdenetwork-filesharing-22.04.1.tar.xz
"
